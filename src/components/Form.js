import { Formik, Form, Field, ErrorMessage, FieldArray } from "formik";
import React from "react";
import * as Yup from "yup";

const onSubmit = (values) => {
  console.log("Form data", values);
};
const FormikForm = () => {
  const initialValues = {
    name: "",
    address: "",
    email: "",
    phoneNumbers: "",
    eduDetails: [{ boardName: "", passedLevel: "", passedYear: "" }],
  };

  const validationSchema = Yup.object({
    name: Yup.string().required("this field is required"),
    address: Yup.string().required("required"),
    email: Yup.string().email("invalid Format").required("Required!"),
  });

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={validationSchema}
      onSubmit={onSubmit}
    >
      <Form>
        <div>
          <label>Name:</label>
          <Field type="text" id="name" name="name" />
          <ErrorMessage name="name" />
        </div>
        <br />
        <br />
        <div>
          <label>Address:</label>
          <Field type="text" id="address" name="address" />
          <ErrorMessage name="address" />
        </div>
        <br />
        <br />
        <div>
          <label>Email:</label>
          <Field type="email" id="email" name="email" />
          <ErrorMessage name="email" />
        </div>
        <br />
        <br />
        <div>
          <label>Phone Number</label>
          <Field type="contact" id="contact" name="contact" />
          <ErrorMessage name="contact" />
        </div>
        <br />
        <br />
        <div>
          <label>Educational Details</label>
          <FieldArray name="eduDetails">
            {(fieldArrayProps) => {
              console.log("fieldArrayProps", fieldArrayProps);
              const { push, remove, form } = fieldArrayProps;
              const { values } = form;
              const { eduDetails } = values;
              return (
                <div>
                  {eduDetails.map((eduDetails, index) => (
                    <div key={index}>
                      <label htmlFor="">Board Name</label>
                      <Field
                        placeholder="Board Name"
                        label="Board Name"
                        name={`eduDetails[${index}.boardName]`}
                      />{" "}
                      <label htmlFor="">Passed Level</label>
                      <Field
                        placeholder="Passed Level"
                        label="Passed Level"
                        name={`eduDetails[${index}].passedLevel`}
                      />{" "}
                      <label htmlFor="">Passed Year</label>
                      <Field
                        placeholder="Passed Level"
                        label="Passed Level"
                        name={`eduDetails[${index}].passedYear`}
                      />
                      <button type="button" onClick={() => remove(index)}>
                        Delete
                      </button>
                      <button
                        type="button"
                        onClick={() =>
                          push({
                            boardName: "",
                            passedLevel: "",
                            passedYear: "",
                          })
                        }
                      >
                        Add
                      </button>
                    </div>
                  ))}
                </div>
              );
            }}
          </FieldArray>
        </div>

        <button type="submit">Submit</button>
      </Form>
    </Formik>
  );
};
export default FormikForm;
